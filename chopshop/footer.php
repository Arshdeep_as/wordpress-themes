			<footer  class="py-5 bg-dark">
				<?php wp_nav_menu(['menu' => 'footer']); ?><br />
				<p class="m-0 text-center text-white">Copyright &copy; 2018 by Arshdeep Singh Rangi</p>
			</footer>
		</div><!-- /container -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<?php wp_footer(); ?>
</body>
</html>