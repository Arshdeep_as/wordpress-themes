<?php get_header(); ?>
	<div class="container">
		<header class="main">
			<img src="<?=get_template_directory_uri()?>/images/header.jpg" alt="Header Image" />
		</header>

		<div id="my_div" class="row">
		  <div class="col-4">
		  	<?php get_sidebar(); ?>
		  </div>
		  <div class="col-8">
		  	<div id="content">
				<div id="primary">			
					<?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<a href="<?php the_permalink() ?>"><?php the_title('<h1>','</h1>'); ?></a>
						<p><small>Posted on <?php the_date();?></small></p>
						<?php the_excerpt('<div>','</div>'); ?>
						
						<?php if(!is_single()) :?>
							<a href="<?php the_permalink(); ?>">READ MORE....</a>
						<?php endif;?>
						<hr />
					<?php endwhile; ?>
				</div><!-- /primary -->
			</div><!-- /content -->	
		  </div>
		</div>
		


<?php get_footer(); ?>
