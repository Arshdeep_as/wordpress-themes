<div id="sidebar">
	<h3>Menu</h3>
	<?php wp_nav_menu(['menu' => 'menu', 'container' => 'ul']); ?>
	<hr />
	<?php echo get_search_form(); ?>
	<hr />
	<h3>Archives</h3>
	<ul class="menu"><?php wp_get_archives() ?></ul>
	<hr />
	<h3>Category</h3>
	<ul class="menu"><?php wp_list_categories('title_li') ?></ul>
</div><!-- /sidebar -->