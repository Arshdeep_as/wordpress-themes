<?php
/*
Theme Name: spacedebris
 */
?><!doctype html>
<html lang="en">
  <head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri();?>" >
	<title>Space Debris</title>
	<?php wp_head(); ?>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
			  <a class="navbar-brand" href="#">Space Debris</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			    <div class="navbar-nav">
			      <a class="nav-item nav-link active" href="#">Home</a>
			      <a class="nav-item nav-link" href="#">Debris</a>
			      <a class="nav-item nav-link" href="#">Tracking</a>
			      <a class="nav-item nav-link" href="#">Tracking</a>
						<a class="nav-item nav-link" href="#">Supplies</a>
						<a class="nav-item nav-link" href="#">Join us</a>
						<a class="nav-item nav-link" href="#">News</a>
			    </div>
	  		</div>
			</nav>

			<h1 style="text-align: center;">Bringing Space Debris Home!</h1>

			<div class="row">
				<div class="image_container container col-md-12">
					<img id="my_image" src="<?=get_template_directory_uri()?>/images/slider.jpg" alt="Slider Image">
				</div>
			</div>

			<div class=" myrow row">
				<div class="container col">

			    <div class="callout">
			      <h2>New Space Junk</h2>
			      <img src="<?=get_template_directory_uri()?>/images/New_Space_Junk.jpg" alt="Slider Image">
			      <p>Oh, how I wish I could believe or understand that! There's only one reasonable course of action now: kill Flexo! Who's brave enough to fly into something we all keep calling a death sphere? It's okay, Bender. I like cooking too.</p>
			      <p><a href="">READ MORE >></a></p>
			    </div>
			  </div>
				<div class="container col">
			    <div class="callout">
			      <h2>Items for sale</h2>
			      <img src="<?=get_template_directory_uri()?>\images\Items_for_sale.jpg" alt="Slider Image">
			      <p>Look, last night was a mistake. Oh, all right, I am. But if anything happens to me, tell them I died robbing some old man. A sexy mistake. Can we have Bender Burgers again?</p>
			      <p><a href="">READ MORE >></a></p>
			    </div>
			  </div>
			  <div class="container col">
			    <div class="callout">
			      <h2>Join Us!</h2>
			      <img src="<?=get_template_directory_uri()?>\images\Join_Us.jpg" alt="Slider Image">
			      <p>You wouldn't. Ask anyway! Does anybody else feel jealous and aroused and worried? Michelle, I don't regret this, but I both rue and lament it. That's the ONLY thing about being a slave.</p>
			      <p><a href="">READ MORE >></a></p>
			    </div>
			  </div>
			</div>
		  </div>

    </div>
	

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<?php wp_footer(); ?>
  </body>
</html>