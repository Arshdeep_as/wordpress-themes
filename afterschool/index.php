<?php get_header(); ?>	

		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12 col-sm-9">
				<h1 class="archive_title">News</h1>
				<?php while(have_posts()) :?>
					<?php the_post(); ?>
					<a href="<?php the_permalink() ?>"><?php the_title('<h1>','</h1>'); ?></a>
					<p><small>Posted on <?php the_date();?></small></p>
					<?php the_excerpt('<div>','</div>'); ?>

					<?php if(!is_single()) :?>
						<a href="<?php the_permalink(); ?>">READ MORE....</a>
					<?php endif;?>
					<hr />
				<?php endwhile; ?>
			</div><!-- /primary -->

			<div id="secondary" class="col-xs-12 col-sm-3">				
				<?php get_sidebar(); ?>
			</div><!-- /secondary -->

		</div><!-- /content -->

		

	
<?php get_footer(); ?>
	