<?php
if(function_exists('add_theme_support'))
{
	add_theme_support('post-thumbnails');
	add_image_size('list-thumb', 75, 75);
}

if(!function_exists('register_nav_menus')){
	register_nav_menus();
}

if(!function_exists('my_load_styles'))
{
	function my_load_styles()
	{
		wp_enqueue_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', [], '4.1.3', 'all');

		wp_enqueue_style('afterschool', get_stylesheet_uri(), [], false, 'all');

		wp_enqueue_script('bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', ['jquery'], '4.1.3', true);
	}

	add_action('wp_enqueue_scripts', 'my_load_styles');
}