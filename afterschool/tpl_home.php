<?php 
/*
Template Name: home
*/
get_header(); ?>
	

		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12">
				<?php $items = get_posts(['category_name' => 'slides']); ?>		
				<?php foreach($items as $item): ?>
					<div id="featured" class="col-xs-12">
								
						<?php the_post_thumbnail();?>
					
						<div class="caption col-xs-12">
							<a href="#">Find out about our programs for children</a>
						</div><!-- /caption -->
						
					</div><!-- /featured -->
				<?php endforeach; ?>
			</div><!-- /primary -->

			<div class="col-xs-12">				
				<?php get_sidebar('home'); ?>
			</div><!-- /secondary -->

		</div><!-- /content -->

		

	
<?php get_footer(); ?>
	