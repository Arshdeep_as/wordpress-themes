<?php get_header(); ?>	
		<div id="content" class="col-xs-12">
			<div id="primary" class="col-xs-12 col-sm-9">
				<?php while(have_posts()) : ?>
						<?php the_post(); ?>
						<?php the_title('<h1>','</h1>'); ?>
						<p><small>Posted on <?php the_date();?></small></p>
						<?php the_content('<div>','</div>'); ?>
						<hr />
					<?php endwhile; ?>
					<?php comments_template(); ?>
				
			</div><!-- /primary -->

			<div id="secondary" class="col-xs-12 col-sm-3">				
				<?php get_sidebar(); ?>
			</div><!-- /secondary -->

		</div><!-- /content -->

		

	
<?php get_footer(); ?>
	